package pl.krzysztof.lipka.laborki.main.coordinates

import dagger.Binds
import dagger.Module

@Module
abstract class CoordinatesFragmentModule {

    @Binds
    abstract fun bindsCoordinatesView(
        CoordinatesFragment: CoordinatesFragment
    ): CoordinatesView
}
