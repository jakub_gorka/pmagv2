package pl.krzysztof.lipka.laborki.common.utils.alert_dialog

import android.app.Activity
import androidx.fragment.app.FragmentActivity
import pl.krzysztof.lipka.laborki.main.coordinates.CoordinatesFragment
import javax.inject.Inject
import javax.inject.Singleton
import androidx.fragment.app.Fragment
import pl.krzysztof.lipka.laborki.R
import pl.krzysztof.lipka.laborki.main.recipient_data.RecipientDataFragment

interface ScreenNavigator {
    fun navigateToRecipientDataFragment(activity: FragmentActivity)
    fun navigateToCoordinateFragment(activity: FragmentActivity)
}

@Singleton
class ScreenNavigatorImpl @Inject constructor() : ScreenNavigator {
    override fun navigateToCoordinateFragment(activity: FragmentActivity) {
        showFragment(CoordinatesFragment(), activity)
    }

    override fun navigateToRecipientDataFragment(activity: FragmentActivity) {
        showFragment(RecipientDataFragment(), activity)
    }

    private fun showFragment(fragment: Fragment, activity: FragmentActivity) =
        activity.supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragment_container, fragment)
            .addToBackStack(null)
            .commit()
}