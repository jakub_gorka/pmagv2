package pl.krzysztof.lipka.laborki.data

import javax.inject.Inject
import javax.inject.Singleton

interface Repository {
    fun saveMail(mail: String)
    fun saveCoords(lat: String, lon: String)
    fun getData(): RepositoryData
}

@Singleton
class RepositoryImpl @Inject constructor(): Repository {
    private var data : RepositoryData = RepositoryData()
    override fun saveMail(mail: String) {
        data.mail = mail
    }

    override fun saveCoords(lat: String, lon: String) {
        data.lat = lat.toDouble()
        data.lon = lon.toDouble()
    }

    override fun getData(): RepositoryData {
        return data
    }
}