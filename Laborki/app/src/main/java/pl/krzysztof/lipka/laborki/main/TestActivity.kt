package pl.krzysztof.lipka.laborki.main

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_recipient_data.*
import pl.krzysztof.lipka.laborki.R
import pl.krzysztof.lipka.laborki.common.BaseActivity
import pl.krzysztof.lipka.laborki.common.BaseFragment
import pl.krzysztof.lipka.laborki.main.recipient_data.RecipientDataPresenter
import pl.krzysztof.lipka.laborki.main.recipient_data.RecipientDataView
import javax.inject.Inject

class TestFragment : BaseFragment() {

    @Inject
    lateinit var presenter: RecipientDataPresenter

    companion object {
        private const val REQUEST_CODE_ACCESS_FINE_PERMISSION = 1
        private const val REQUEST_CODE_GPS_SETTINGS = 2137
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_recipient_data, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.let { baseActivity ->
            if (ActivityCompat.checkSelfPermission(
                    baseActivity,
                    android.Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
            ) {

            } else {
                requestPermissions(
                    arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                    REQUEST_CODE_ACCESS_FINE_PERMISSION

                )
            }
        }
    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Log.d("a","result2")
        if(requestCode == REQUEST_CODE_ACCESS_FINE_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            activity?.let {
                baseActivity ->

                val locationManager = context!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager

                Log.d("a","result3")
                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

                } else {
                    baseActivity.startActivityForResult(
                        Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS),
                        REQUEST_CODE_GPS_SETTINGS)
                }
        }
    }
//        next_button.setOnClickListener {
//            presenter.saveEmail(email_text.text.toString())
//            presenter.navigate(this)
//        }
    }
}