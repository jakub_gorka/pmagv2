package pl.krzysztof.lipka.laborki.main.coordinates

import android.app.Activity
import android.location.Location
import android.util.Log
import android.widget.Toast
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_recipient_data.*
import pl.krzysztof.lipka.laborki.R
import pl.krzysztof.lipka.laborki.common.utils.alert_dialog.ScreenNavigator
import pl.krzysztof.lipka.laborki.common.utils.gps.GpsUtils
import pl.krzysztof.lipka.laborki.common.utils.gps.LocationUtils
import pl.krzysztof.lipka.laborki.common.utils.gps.PermissionUtils
import pl.krzysztof.lipka.laborki.data.Repository
import javax.inject.Inject

class CoordinatesPresenter @Inject constructor(
    private val view: CoordinatesView,
    private val permissionUtils: PermissionUtils,
    private val gpsUtils: GpsUtils,
    private val locationUtils: LocationUtils,
    private val repository: Repository
//    private val screenNavigator: ScreenNavigator
) {

    fun onViewCreated(fragment: Fragment) {
        Log.d("on view created","before")
        if (checkFineLocationPermission()) {
            Log.d("on view created","fine location permission")
            if (isOn()) {
                fragment.activity?.let {
                    getLocation(it)
                }
            } else {
                fragment.context?.let {
                    Toast.makeText(it, R.string.make_sure_gps_is_on_high, Toast.LENGTH_LONG).show()
                }
                gpsUtils.openSettings(fragment)
            }
        } else {
            permissionUtils.requestAccessFineLocationPermission(fragment)
        }
    }

    fun isValidReturn(requestCode: Int): Boolean {
        return gpsUtils.isValidReturn(requestCode)
    }
    fun isOn(): Boolean {
        return gpsUtils.isOn()
    }
    fun getLocation(activity: Activity) {
        locationUtils.getLocation(activity) { location: Location ->
            view.onCoordinatesRecieved(location)
            saveCoords(location.longitude.toString(), location.latitude.toString())
        }
    }
    fun checkFineLocationPermission(): Boolean {
        return permissionUtils.checkFineLocationPermission()
    }
    fun getLocation(fragment: Fragment) {
        if (isOn()) {
            fragment.activity?.let {
                getLocation(it)
            }
        }
        else {
            goToSettingsAndToast(fragment)
        }
    }
    private fun goToSettingsAndToast(fragment: Fragment) {
        fragment.context?.let {
            Toast.makeText(it, R.string.make_sure_gps_is_on_high, Toast.LENGTH_LONG).show()
        }
        gpsUtils.openSettings(fragment)
    }

    fun saveCoords(lat: String, lon: String) {
        repository.saveCoords(lat, lon)
    }

    fun getLatFromRepo() : String {
        return repository.getData().lat?.toString() ?: ""
    }

    fun getLonFromRepo() : String {
        return repository.getData().lon?.toString() ?: ""
    }

//    fun navigate(fragment: Fragment) {
//        fragment.activity?.let {
//            screenNavigator.navigateToNextFragment(it)
//        }
//    }
}
