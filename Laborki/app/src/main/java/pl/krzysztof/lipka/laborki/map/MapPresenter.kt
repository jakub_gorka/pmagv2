package pl.krzysztof.lipka.laborki.map

import android.content.Context
import android.widget.PopupWindow
import androidx.annotation.DrawableRes
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import pl.krzysztof.lipka.laborki.R
import pl.krzysztof.lipka.laborki.data.Repository
import pl.krzysztof.lipka.laborki.data.ResourcesRepository
import pl.krzysztof.lipka.laborki.data.model.Resource
import javax.inject.Inject

class MapPresenter @Inject constructor(
    private val view: MapView,
    private val context: Context,
    private val repo: Repository,
    resourcesRepo: ResourcesRepository
) : OnMapReadyCallback {

    companion object {
        const val INITIAL_ZOOM = 15f
    }

    var googleMap: GoogleMap? = null
    val resources = resourcesRepo.getResources()
    var selectedResource = resources.findLast { it.isActive }
    var popup: PopupWindow? = null

    override fun onMapReady(map: GoogleMap?) {
        googleMap = map
        setUpMap()

        googleMap?.setOnMapLongClickListener { latLng ->
            val markerOptions = getMarkerOptions(
                latLng, selectedResource!!.name,
                context.getString(
                    R.string.shared_coordinates_format,
                    latLng.latitude.toString(),
                    latLng.longitude.toString()
                ), selectedResource!!.categoryIconResId
            )
            googleMap?.addMarker(markerOptions)
            googleMap?.setOnMarkerClickListener {
            view.showPopUp(selectedResource!!, googleMap?.projection!!.toScreenLocation(it.position))
                true
            }
        }
    }

    private fun deleteMarker(marker: Marker) {
        marker.remove()
    }
    private fun setUpMap() {
        googleMap?.apply {
            repo.getData().lat?.let { latitude ->
                repo.getData().lon?.let { longitude ->
                    val userCoordinates = LatLng(longitude, latitude)
                    moveCamera(
                        CameraUpdateFactory.newLatLngZoom(userCoordinates, INITIAL_ZOOM)
                    )
                    mapType = GoogleMap.MAP_TYPE_NORMAL
                }
            }
        }
    }

    private fun getMarkerOptions(
        coords: LatLng,
        title: String,
        snippet: String, @DrawableRes iconId: Int
    ): MarkerOptions {
        val markerOptions = MarkerOptions()
        markerOptions.position(coords)
        markerOptions.title(title)
        markerOptions.snippet(snippet)
        markerOptions.icon(BitmapDescriptorFactory.fromResource(iconId))
        return markerOptions
    }

    fun onResourceSelected(resource: Resource) {
        selectedResource = resource
    }


}
