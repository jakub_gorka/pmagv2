package pl.krzysztof.lipka.laborki.main.coordinates

import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_coordinates.*
import kotlinx.android.synthetic.main.fragment_recipient_data.*
import pl.krzysztof.lipka.laborki.R
import pl.krzysztof.lipka.laborki.common.BaseFragment
import pl.krzysztof.lipka.laborki.map.MapActivity
import javax.inject.Inject


class CoordinatesFragment : BaseFragment(), CoordinatesView {
    @Inject
    lateinit var presenter: CoordinatesPresenter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_coordinates, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lat_text.setText(presenter.getLatFromRepo())
        lon_text.setText(presenter.getLonFromRepo())
        presenter.onViewCreated(this)

        show_button.setOnClickListener{
            Log.d("test", presenter.getLatFromRepo())
            if (presenter.getLatFromRepo().isNotEmpty()){
                activity?.let{ baseActivity ->
                    val mapActivityIntent = Intent(baseActivity, MapActivity::class.java)
                    startActivity(mapActivityIntent)
                }
            }
        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (presenter.isValidReturn(requestCode) && presenter.isOn()) {
            activity?.let {
                presenter.getLocation(it)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(presenter.checkFineLocationPermission()) {
            presenter.getLocation(this)
        }
    }

    override fun onCoordinatesRecieved(location: Location) {
        lat_text.setText(location.latitude.toString())
        lon_text.setText(location.longitude.toString())
        presenter.saveCoords(lon_text.text.toString(), lat_text.text.toString())
    }
}
