package pl.krzysztof.lipka.laborki.common.utils.gps

import androidx.fragment.app.Fragment
import javax.inject.Inject
import javax.inject.Singleton
import android.content.Context
import android.content.Intent
import android.location.LocationManager
import android.provider.Settings


interface GpsUtils {
    fun isOn(): Boolean
    fun openSettings(fragment: Fragment)
    fun isValidReturn(code: Int): Boolean
}

@Singleton
class GpsUtilsImpl @Inject constructor(
    private val context: Context
): GpsUtils {

    companion object {
        private const val VALID_RETURN = 2137
    }

    override fun isOn(): Boolean {
        val locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    override fun openSettings(fragment: Fragment) {
        fragment.startActivityForResult(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), VALID_RETURN)
    }

    override fun isValidReturn(code: Int): Boolean {
        return code == VALID_RETURN
    }
}