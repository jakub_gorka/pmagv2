package pl.krzysztof.lipka.laborki.main.coordinates

import android.location.Location

interface CoordinatesView {

    fun onCoordinatesRecieved(location: Location)
}
