package pl.krzysztof.lipka.laborki.common.utils

import android.content.Context

fun convertDpToPx(valueInDp: Float, context: Context) =
    (valueInDp * context.resources.displayMetrics.density).toInt()