package pl.krzysztof.lipka.laborki.main

import android.content.Intent

interface MainView {
    fun share(intent: Intent)
}
