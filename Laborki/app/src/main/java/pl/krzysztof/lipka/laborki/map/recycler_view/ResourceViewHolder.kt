package pl.krzysztof.lipka.laborki.map.recycler_view

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_resource.view.*

class ResourceViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val categoryText: TextView = view.element_text
    val categoryImage: ImageView = view.element_image
    val stateIcon: ImageView = view.state_icon
    val container: View = view
}