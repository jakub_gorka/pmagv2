package pl.krzysztof.lipka.laborki.main

import android.content.Context
import pl.krzysztof.lipka.laborki.R
import pl.krzysztof.lipka.laborki.common.utils.ShareUtils
import pl.krzysztof.lipka.laborki.data.Repository
import javax.inject.Inject

class MainPresenter @Inject constructor(
    private val view: MainView,
    private val shareUtils: ShareUtils,
    private val context: Context,
    private val repo: Repository
) {

    fun onSharePressed() {
        val lat = repo.getData().lat
        val lon = repo.getData().lon

        val mail = repo.getData().mail

        lat?.let {latitude->
            lon?.let {longitude->
                val body = context.getString(
                    R.string.shared_coordinates_format,
                    latitude.toString(), longitude.toString()
                )
                if (mail.isNotEmpty()) {
                    view.share(shareUtils.getShareByEmailIntent(mail, body))
                } else {
                    view.share(shareUtils.getShareIntent(body))
                }
            }
        }


    }
}
