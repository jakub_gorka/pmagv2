package pl.krzysztof.lipka.laborki.map

import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class MapActivityModule {

    @Binds
    abstract fun bindsMapView(
        mainActivity: MapActivity
    ): MapView

}
