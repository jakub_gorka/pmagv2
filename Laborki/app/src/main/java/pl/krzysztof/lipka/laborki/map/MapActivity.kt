package pl.krzysztof.lipka.laborki.map

import android.graphics.Point
import android.os.Bundle
import android.view.WindowManager
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.maps.SupportMapFragment
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_map.*
import pl.krzysztof.lipka.laborki.R
import pl.krzysztof.lipka.laborki.common.BaseActivity
import pl.krzysztof.lipka.laborki.common.utils.PopUpWindowUtils
import pl.krzysztof.lipka.laborki.data.ResourcesRepository
import pl.krzysztof.lipka.laborki.data.model.Resource
import pl.krzysztof.lipka.laborki.map.recycler_view.ResourcesAdapter
import javax.inject.Inject


class MapActivity : BaseActivity(), MapView {

    @Inject
    lateinit var resources: ResourcesRepository

    @Inject
    lateinit var presenter: MapPresenter

    @Inject
    lateinit var popupWindowUtils: PopUpWindowUtils

    override fun onCreate(
        savedInstanceState: Bundle?
    ) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setTheme(R.style.AppTheme)
        setContentView(R.layout.activity_map)
        val viewManager = LinearLayoutManager(this)
        val viewAdapter = ResourcesAdapter(presenter.resources) {
            presenter.onResourceSelected(it)
        }

        my_recycler_view.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }
        val dividerItemDecoration = DividerItemDecoration(my_recycler_view.context, viewManager.orientation)
        my_recycler_view.addItemDecoration(dividerItemDecoration)

        window.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        )

        val mapFragment = SupportMapFragment.newInstance()
        supportFragmentManager.beginTransaction()
            .add(R.id.map_container, mapFragment).commit()
        mapFragment.getMapAsync(presenter)
    }

    override fun showPopUp(selectedResource: Resource, position: Point) {
//        popup?.dismiss()
        val popup = popupWindowUtils.createPopUpWindow(
            title = selectedResource.name,
            snippet = selectedResource.specification,
            photoId = selectedResource.imageResId,
            onRemoveClicked = {
                marker.remove()
            }
        )
        popupWindowUtils.showPopUp(position, popup, map_container)
    }
}
