package pl.krzysztof.lipka.laborki.data.model


enum class ResourceType(val value: String) {
    LAND_FORCES("LAND_FORCES"),
    MARINE_FLEET("MARINE_FLEET"),
}