package pl.krzysztof.lipka.laborki.map.recycler_view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import pl.krzysztof.lipka.laborki.R
import pl.krzysztof.lipka.laborki.data.model.Resource

class ResourcesAdapter(
    private val resources: List<Resource>,
    private val onResourceSelected: (res: Resource) -> Unit
) : RecyclerView.Adapter<ResourceViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ResourceViewHolder = ResourceViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_resource, parent, false)
    )

    override fun onBindViewHolder(
        holder: ResourceViewHolder,
        position: Int
    ) {
        holder.apply {
            val resource = resources[position]
            categoryImage.setImageResource(resource.categoryIconResId)
            categoryText.text = resource.name
            toggleStateIconVisibility(resource.isActive)
            container.setOnClickListener {
                onContainerClicked(resource)
            }
        }
    }

    private fun ResourceViewHolder.toggleStateIconVisibility(
        isActive: Boolean
    ) {
        stateIcon.visibility =
                if (isActive) {
                    View.VISIBLE
                } else {
                    View.INVISIBLE
                }
    }

    private fun onContainerClicked(
        resource: Resource
    ) {
        if (!resource.isActive) {
            resources.firstOrNull { it.isActive }?.isActive = false
            resource.isActive = true
            onResourceSelected(resource)
            notifyDataSetChanged()
        }
    }

//    fun getActivResource() : Resource? {
//        return resources.firstOrNull { it.isActive }
//    }

    override fun getItemCount() = resources.size
}