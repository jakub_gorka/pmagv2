package pl.krzysztof.lipka.laborki.common.utils

import android.content.Context
import android.graphics.Point
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.PopupWindow
import androidx.annotation.ContentView
import androidx.annotation.DrawableRes
import com.google.android.gms.maps.Projection
import com.google.android.gms.maps.model.Marker
import kotlinx.android.synthetic.main.pop_up.view.*
import pl.krzysztof.lipka.laborki.R
import javax.inject.Inject
import javax.inject.Singleton

interface PopUpWindowUtils {

    fun createPopUpWindow(
        title: String,
        snippet: String,
        @DrawableRes photoId: Int,
        onRemoveClicked: () -> Unit
    ): PopupWindow

    fun showPopUp(position: Point, popup: PopupWindow, container: View)
}

@Singleton
class PopUpWindowUtilsImpl @Inject constructor(
    private val context: Context
) : PopUpWindowUtils {

    override fun createPopUpWindow(
        title: String,
        snippet: String,
        @DrawableRes photoId: Int,
        onRemoveClicked: () -> Unit
    ): PopupWindow {
        val view = LayoutInflater.from(context).inflate(R.layout.pop_up, null, false)
        view.title_text.text = title
        view.snippet_text.text = snippet
        view.resource_image.setImageResource(photoId)
        view.close_button_image.setOnClickListener {
            onRemoveClicked()
        }
        return PopupWindow(view, convertDpToPx(300f, context), convertDpToPx(180f, context))
    }

    override fun showPopUp(position: Point, popup: PopupWindow, container: View) {
//        val position = projection.toScreenLocation(marker.position)
        val offsetX = convertDpToPx(180F, context)
        val offsetY = convertDpToPx(360F, context)
        popup.showAtLocation(container, Gravity.CENTER, position.x - offsetX, position.y - offsetY)
    }

//    private fun convertDpToPx(valueInDp: Float) = (valueInDp * context.resources.displayMetrics.density).toInt()
}


//
//Wypełnić widok informacjami odczytanami z obiektu klasy Resource.
//Wyświetlić okno: popupWindow.showAtLocation(map_container, Gravity.CENTER, position.x - offsetX, position.y - offsetY)
//
//map_container to id kontenera z mapą (w pliku activity_map.xml)
//Parametr o nazwie position to pozycja markera na ekranie. Należy ją odczytać w następujący sposób: projection.toScreenLocation(marker.position). Obiekt o nazwie projection to drugi parametr funkcji onMarkerClicked.
//Parametry offsetX oraz offsetY należy obliczyć według wzoru:
//
//offsetX: convertDpToPx(180F) offsetY: convertDpToPx(360F)