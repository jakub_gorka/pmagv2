package pl.krzysztof.lipka.laborki.main.recipient_data

import androidx.fragment.app.Fragment
import pl.krzysztof.lipka.laborki.common.utils.alert_dialog.ScreenNavigator
import pl.krzysztof.lipka.laborki.data.Repository
import javax.inject.Inject

class RecipientDataPresenter @Inject constructor(
    private val view: RecipientDataView,
    private val screenNavigator: ScreenNavigator,
    private val repository: Repository
) {
    fun navigate(fragment: Fragment) {
        fragment.activity?.let {
            screenNavigator.navigateToCoordinateFragment(it)
        }
    }

    fun saveEmail(email: String) {
        repository.saveMail(email)
    }

    fun getEmailFromRepo() : String {
        return repository.getData().mail
    }

}
