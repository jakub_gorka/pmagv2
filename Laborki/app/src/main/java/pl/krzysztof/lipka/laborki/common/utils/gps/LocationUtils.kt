package pl.krzysztof.lipka.laborki.common.utils.gps

import android.app.Activity
import android.content.Context
import android.location.Location
import com.google.android.gms.location.*
import javax.inject.Inject
import javax.inject.Singleton

interface LocationUtils {
    fun getLocation(activity: Activity, callback: (location: Location) -> Unit)
}

@Singleton
class LocationUtilsImpl @Inject constructor() : LocationUtils {

    companion object {
        private const val minTime = 10000L
        private const val minDist = 10000.0f
    }

    private fun getLocationCallback(callback: (location: Location) -> Unit) : LocationCallback {
        return object : LocationCallback() {

            override fun onLocationResult(
                locationResult: LocationResult?
            ) {
                locationResult?.lastLocation?.let {
                    callback(it)
                }
            }
        }
    }

    override fun getLocation(activity: Activity, callback: (location: Location) -> Unit) {
        try {
            val locationRequest = LocationRequest().apply {
                smallestDisplacement = minDist
                interval = minTime
            }

            val fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(activity)
            fusedLocationProviderClient.requestLocationUpdates(locationRequest, getLocationCallback(callback), null)
        }
        catch (exception: SecurityException) { }
    }
}