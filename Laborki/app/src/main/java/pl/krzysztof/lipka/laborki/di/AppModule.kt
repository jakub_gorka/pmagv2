package pl.krzysztof.lipka.laborki.di

import android.app.Application
import android.content.Context
import dagger.Binds
import dagger.Module
import dagger.android.support.AndroidSupportInjectionModule
import pl.krzysztof.lipka.laborki.App
import pl.krzysztof.lipka.laborki.common.utils.PopUpWindowUtils
import pl.krzysztof.lipka.laborki.common.utils.PopUpWindowUtilsImpl
import pl.krzysztof.lipka.laborki.common.utils.ShareUtils
import pl.krzysztof.lipka.laborki.common.utils.ShareUtilsImpl
import pl.krzysztof.lipka.laborki.common.utils.alert_dialog.AlertDialogUtils
import pl.krzysztof.lipka.laborki.common.utils.alert_dialog.AlertDialogUtilsImpl
import pl.krzysztof.lipka.laborki.common.utils.alert_dialog.ScreenNavigator
import pl.krzysztof.lipka.laborki.common.utils.alert_dialog.ScreenNavigatorImpl
import pl.krzysztof.lipka.laborki.common.utils.gps.*
import pl.krzysztof.lipka.laborki.data.Repository
import pl.krzysztof.lipka.laborki.data.RepositoryImpl
import pl.krzysztof.lipka.laborki.data.ResourcesRepository
import pl.krzysztof.lipka.laborki.data.ResourcesRepositoryImpl

@Module(includes = [AndroidSupportInjectionModule::class, ActivityModule::class])
abstract class AppModule {

    @Binds
    abstract fun bindsApplication(
        app: App
    ): Application

    @Binds
    abstract fun bindsContext(
        app: App
    ): Context

    @Binds
    abstract fun bindsAlertDialogUtils(
        alertDialogUtilsImpl: AlertDialogUtilsImpl
    ): AlertDialogUtils

    @Binds
    abstract fun bindsGpsUtils(
        gpsUtilsImpl: GpsUtilsImpl
    ): GpsUtils

    @Binds
    abstract fun bindsLocationUtils(
        locationUtilsImpl: LocationUtilsImpl
    ): LocationUtils

    @Binds
    abstract fun bindsPermissionUtils(
        permissionUtilsImpl: PermissionUtilsImpl
    ): PermissionUtils

    @Binds
    abstract fun bindsNavigator(
        screenNavigator: ScreenNavigatorImpl
    ): ScreenNavigator

    @Binds
    abstract fun bindsRepository(
        repository: RepositoryImpl
    ): Repository

    @Binds
    abstract fun bindsShareUtils(
        shareUtils: ShareUtilsImpl
    ): ShareUtils

    @Binds
    abstract fun bindsResourcesRepository(
        resourcesRepository: ResourcesRepositoryImpl
    ): ResourcesRepository

    @Binds
    abstract fun bindsPopUpWindowUtils(
        popUpWindowUtils: PopUpWindowUtilsImpl
    ): PopUpWindowUtils
}
