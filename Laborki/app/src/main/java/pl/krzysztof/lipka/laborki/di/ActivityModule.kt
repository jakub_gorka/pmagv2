package pl.krzysztof.lipka.laborki.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pl.krzysztof.lipka.laborki.main.MainActivity
import pl.krzysztof.lipka.laborki.main.MainActivityModule
import pl.krzysztof.lipka.laborki.map.MapActivity
import pl.krzysztof.lipka.laborki.map.MapActivityModule

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    abstract fun contributesMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [MapActivityModule::class])
    abstract fun contributesMapActivity(): MapActivity
}
