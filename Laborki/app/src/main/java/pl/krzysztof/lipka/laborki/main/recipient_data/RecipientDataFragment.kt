package pl.krzysztof.lipka.laborki.main.recipient_data

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_recipient_data.*
import pl.krzysztof.lipka.laborki.R
import pl.krzysztof.lipka.laborki.common.BaseFragment
import android.widget.Button
import pl.krzysztof.lipka.laborki.data.Repository
import pl.krzysztof.lipka.laborki.main.MainActivity
import pl.krzysztof.lipka.laborki.main.coordinates.CoordinatesFragment
import javax.inject.Inject


class RecipientDataFragment : BaseFragment(), RecipientDataView {

    @Inject
    lateinit var presenter: RecipientDataPresenter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_recipient_data, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        email_text.setText(presenter.getEmailFromRepo())
        next_button.setOnClickListener {
            presenter.saveEmail(email_text.text.toString())
            presenter.navigate(this)
        }
    }
}
