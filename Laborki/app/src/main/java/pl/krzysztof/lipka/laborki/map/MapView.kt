package pl.krzysztof.lipka.laborki.map

import android.graphics.Point
import pl.krzysztof.lipka.laborki.data.model.Resource

interface MapView {

    fun showPopUp(selectedResource: Resource, position: Point)

}
