package pl.krzysztof.lipka.laborki.common.utils

import android.content.Context
import android.content.Intent
import android.net.Uri
import pl.krzysztof.lipka.laborki.R
import javax.inject.Inject

interface ShareUtils {
    fun getShareByEmailIntent(mail: String, body: String) : Intent
    fun getShareIntent(body: String) : Intent
}

class ShareUtilsImpl @Inject constructor(private val context: Context): ShareUtils {

    companion object {
        private const val TYPE_TEXT = "type/text"
    }

    override fun getShareByEmailIntent(mail: String, body: String): Intent {
        val intent = Intent(Intent.ACTION_SENDTO)
        val encodedBody = Uri.encode(body)
        val encodedSubject = Uri.encode(context.getString(R.string.app_name))
        val mailtoUriString = context.getString(R.string.mailto_uri_format, mail, encodedSubject, encodedBody)
        intent.data = Uri.parse(mailtoUriString)
        return intent
    }

    override fun getShareIntent(body: String): Intent {
        val intent = Intent(Intent.ACTION_SEND)
        intent.putExtra(Intent.EXTRA_TEXT, body)
        intent.type = TYPE_TEXT
        return intent
    }
}