package pl.krzysztof.lipka.laborki.common.utils.gps

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import javax.inject.Inject
import javax.inject.Singleton

interface PermissionUtils {
    fun checkFineLocationPermission(): Boolean
    fun requestAccessFineLocationPermission(fragment: Fragment)
}


@Singleton
class PermissionUtilsImpl @Inject constructor(
    private val context: Context
) : PermissionUtils {

    companion object {
        private const val REQUEST_CODE_ACCESS_FINE_LOCATION_PERMISSION = 1
    }

    override fun checkFineLocationPermission(): Boolean {
        val permission = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
        return permission == PackageManager.PERMISSION_GRANTED
    }

    override fun requestAccessFineLocationPermission(fragment: Fragment) {
        val permissions = arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION)
        fragment.requestPermissions(permissions, REQUEST_CODE_ACCESS_FINE_LOCATION_PERMISSION)
    }
}